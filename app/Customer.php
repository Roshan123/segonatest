<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
    	'name','item_id','email' ,'age','gender','contact_number'
    ];
    public function item(){
    	return $this->belongsTo('App\Item');
    }
}
