<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
	
	protected $fillable = [
		'name','description','created_at_date' ,'image'
	];

	public function customer(){
		return $this->hasMany('App\Customer');
	}
}
