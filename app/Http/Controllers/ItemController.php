<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ItemValidation;
use App\Item;
use Input;
use File;

class ItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items=Item::select('id','name','description','created_at_date','image')->paginate('10');
        return view('admin.item.index',compact('items'));
    }

    public function create()
    {
        return view('admin.item.create');
    }

    public function store(ItemValidation $request)
    {
        $itemDetails=$request->all();
        if($request->hasFile('image')){
          $image=Input::file('image');
          $image->move('uploads',$image->getClientOriginalName());
          $itemDetails['image']=$image->getClientOriginalName();
        }
        Item::create($itemDetails);
        return redirect()->route('admin.item')->with(['success'=>'Created successfully']);
    }

    public function edit($id)
    {
        $item=Item::findOrFail($id);
        return view('admin.item.edit',compact('item'));
    }

    public function update(Request $request,$id)
    {
        $itemDetails=$request->all();
        $item=Item::findOrFail($id);
        if($request->hasFile('image')){
            $itemImage = public_path("/uploads/$item->image"); 
            if (File::exists($itemImage)) {
                unlink($itemImage);
            }
            $image=Input::file('image');
            $image->move('uploads',$image->getClientOriginalName());
            $itemDetails['image']=$image->getClientOriginalName();
        }
        $item->update($itemDetails);
        return redirect()->route('admin.item')->with(['success'=>'Created successfully']);
    }

    public function search(Request $request){
        $value=$request->input('value');
        $query=Item::select();
        if($value!=''){
            $items=$query->where('name','LIKE','%'.$value.'%')
            ->get();
            echo view('admin.layouts.listitem',compact('items'));
        }
    } 


    public function destroy($id)
    {
        $item=Item::findOrFail($id);
        unlink(public_path().'/uploads/'.$item->image);
        $item->delete();
        $item->customer()->delete();
        return back()->with(['delete'=>'Deleted Successfully']);
    }
}
