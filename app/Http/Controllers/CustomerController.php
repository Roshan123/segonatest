<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CustomerValidation;
use App\User;
use App\Item;
use App\Customer;
use Input;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $customers=Customer::with('item')->paginate('10');
      return view('admin.customer.index',compact('customers'));
    }

    public function create()
    {
      $items=Item::select('name','id')->get();
      return view('admin.customer.create',compact('items'));
    }

    public function store(CustomerValidation $request)
    {
     $customerDetails=$request->all();
     $customerDetails['item_id']=$request->input('item');
     Customer::create($customerDetails);
     return redirect()->route('admin.customer')->with(['success'=>'Created successfully']);
   }

   public function edit($id)
   {
    $customer=Customer::with('item')->findOrFail($id);
    $items=Item::select('id','name')->get();
    return view('admin.customer.edit',compact('customer','items'));
  }

  public function update(Request $request,$id)
  {   
    $customerDetails=$request->all();
    $customerDetails['item_id']=$request->input('item');
    Customer::findOrFail($id)->update($customerDetails);
    return redirect()->route('admin.customer')->with(['success'=>'Created successfully']);
  }

  public function search(Request $request){
    $value=$request->input('value');
    $query=Customer::select('*');
    if($value!=''){
      $customers=$query->where('name','LIKE','%'.$value.'%')
      ->orwhere('contact_number','LIKE','%'.$value.'%')
      ->get();
      echo view('admin.layouts.listcustomer',compact('customers'));
    }
  }

  public function destroy($id)
  {
    $customer=Customer::findOrFail($id);
    $customer->delete();
    return back()->with(['delete'=>'Deleted Successfully']); 
  }
}
