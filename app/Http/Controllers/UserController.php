<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserValidation;
use App\User;
use Input;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::select('id','name','email','image')->paginate('10');
        return view('admin.user.index',compact('users'));
    }

    public function create()
    {
        return view('admin.user.create');
    }

    public function store(UserValidation $request)
    {
        $userDetails=$request->all();
        if($request->hasFile('image')){
          $image=Input::file('image');
          $image->move('uploads',$image->getClientOriginalName());
          $userDetails['image']=$image->getClientOriginalName();
        }
        User::create($userDetails);
        return redirect()->route('admin.user')->with(['success'=>'Created successfully']);
    }
}
