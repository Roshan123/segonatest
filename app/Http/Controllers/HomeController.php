<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Customer;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $item_total=Item::count();
         $customer_total=Customer::count();
        return view('admin.index',compact('item_total','customer_total'));
    }
    public function logout(){   
        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect('/');
    }
}
