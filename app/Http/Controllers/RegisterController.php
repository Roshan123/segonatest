<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterValidation;
use App\User;
use App\Item;
use App\Customer;
use Hash;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function store(RegisterValidation $request)
    {
       $userDetails=$request->all();
       $userDetails['password']=Hash::make($request->input('password'));
       User::create($userDetails);
       return redirect()->route('login');
    }
    public function barChart(){
        $item_total=Item::count();
        $customer_total=Customer::count();
        return response()->json(['item_total'=>$item_total,'customer_total'=>$customer_total]);
    }
    
}
