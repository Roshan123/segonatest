<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::post('register', 'RegisterController@store');

Route::group(['middleware' => 'auth'], function () {
	Route::get('/logout', 'HomeController@logout')->name('admin.logout');
	Route::get('/bar_chart', 'RegisterController@barChart');

});


Route::group(['prefix'=>'admin','middleware' => 'auth'], function () {

	Route::get('home', 'HomeController@index')->name('admin.home');

	Route::get('item', 'ItemController@index')->name('admin.item');
	Route::get('item/create', 'ItemController@create')->name('admin.item.create');
	Route::get('admin/item/edit/{id}', 'ItemController@edit')->name('admin.item.edit');
	Route::put('item/{id}', 'ItemController@update')->name('admin.item.update');
	Route::delete('item/{id}', 'ItemController@destroy')->name('admin.item.destroy');
	Route::post('item/search', 'ItemController@search')->name('admin.item.search');
	Route::post('item', 'ItemController@store')->name('admin.item.store');


	Route::get('customer', 'CustomerController@index')->name('admin.customer');
	Route::get('customer/create', 'CustomerController@create')->name('admin.customer.create');
	Route::get('customer/edit/{id}', 'CustomerController@edit')->name('admin.customer.edit');
	Route::put('customer/{id}', 'CustomerController@update')->name('admin.customer.update');
	Route::delete('customer/{id}', 'CustomerController@destroy')->name('admin.customer.destroy');
	Route::post('customer/search', 'CustomerController@search')->name('admin.customer.search');
	Route::post('customer', 'CustomerController@store')->name('admin.customer.store');

});