$(document).ready(function(){
	$("#search_item").on('click',function(){
		var value=$("#search").val();
		$.ajax({ 
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url:'/admin/item/search',
			type:'POST',
			data:{'value':value},
			success:function(data){
				$('#datamodification').html(data);
			},
			error:function(){
			}
		});
	});
	$("#search_customer").on('click',function(){
		var value=$("#search").val();
		$.ajax({ 
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url:'/admin/customer/search',
			type:'POST',
			data:{'value':value},
			success:function(data){
				$('#datamodification').html(data);
			},
			error:function(){
			}
		});
	});
});