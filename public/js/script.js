$(document).ready(function(){
	$("#register").on('click',function(){
		$.ajax({ 
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			url:'/register',
			type:'POST',
			data:{
				'name':$("#name").val(),
				'email':$("#email").val(),
				'password':$("#password").val(),
				'confirm_password':$("#confirm_password").val(),
			},
			success:function(response){
				window.location = "/";

			},
			error: function(data)
			{
				var errors = '';
				for(datas in data.responseJSON){
					errors += data.responseJSON[datas] + '<br>';
				}
				$('#response').show().html(errors); 
			}
		});
	});
});