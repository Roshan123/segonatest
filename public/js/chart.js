$(document).ready(function(){
    var url= window.location.origin+'/bar_chart';
$.get(url, function (result) {
     var data=[];
    data.push(result.item_total);
var ctx = document.getElementById("barChart");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['item'],
        datasets: [{
            label:'Bar Chart',
            data: data,
            backgroundColor: [
            'rgb(0, 128, 255)',
                ],
                borderColor: [
                'rgb(0, 128, 255)',
                ],
                borderWidth: 1
            }]
        },
    });
});
$.get(url, function (result) {
     var data=[];
    data.push(result.item_total);
    data.push(result.customer_total);
var ctx = document.getElementById("pieChart");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ['item','customer'],
        datasets: [{
            label: 'Pie Chart',
            data: data,
            backgroundColor: [
             'rgba(0,0,255)',
                ],
                borderColor: [
                'rgba(0,0,255)',
                ],
                borderWidth: 1
            }]
        },

    });
});
});
