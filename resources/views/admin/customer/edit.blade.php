@extends('admin.layouts.main')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      General Form Elements
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Customer</h3>
        </div>
        <p id="response"></p>
        <form role="form"  class="form-horizontal" method="POST" action="{{route('admin.customer.update',$customer->id)}}" >
          {{csrf_field()}}
          {{method_field('PUT')}}
          <div class="box-body">
            <div class="form-group">
              <label  class="col-sm-2 control-label">Name</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" value="{{$customer->name}}" name="name"  placeholder="Enter Name">
                @if($errors->has('name'))<p id="error">{{$errors->first('name')}}</p>@endif
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Email</label>
              <div class="col-sm-8">
                <input type="email" class="form-control" name="email" value="{{$customer->email}}" placeholder="Enter email">
                @if($errors->has('email'))<p id="error">{{$errors->first('email')}}</p>@endif
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"> Item</label>
              <div class="col-sm-8">
                <select name="item" class="form-control">
                  @foreach($items as $item)
                  <option value="{{$item->id}}" @if($item->name == $customer->item->name) selected @endif>{{$item->name}}</option>
                  @endforeach
                </select>
                @if($errors->has('item'))<p id="error">{{$errors->first('item')}}</p>@endif
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Contact Number</label>
              <div class="col-sm-8">
                <input type="tel" class="form-control" name="contact_number" value="{{$customer->contact_number}}" placeholder="Enter contact number">
                @if($errors->has('contact_number'))<p id="error">{{$errors->first('contact_number')}}</p>@endif
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Age</label>
              <div class="col-sm-8">
                <input type="number" class="form-control" name="age" value="{{$customer->age}}"  min="15" max="45">
                @if($errors->has('age'))<p id="error">{{$errors->first('age')}}</p>@endif
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Gender</label>
              <div class="col-sm-8">
                <input type="radio"  name="gender" {{ $customer->gender == 'male' ? 'checked' : ''}} value="male"  />Male
                <input type="radio"  name="gender" {{ $customer->gender == 'female' ? 'checked' : ''}} value="Female"  />Female
                @if($errors->has('gender'))<p id="error">{{$errors->first('gender')}}</p>@endif
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="box-footer">
            <button type="submit" class="btn btn-primary col-sm-offset-2">Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
</section>
</div>
@endsection

