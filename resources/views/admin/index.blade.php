@extends('admin.layouts.main')
@section('content')
<div class="content-wrapper">
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
          <div class="inner">
            <h3>{{$item_total}}</h3>

            <p>Items</p>
          </div>
          <div class="icon">
            <i class="ion ion-bag"></i>
          </div>
          <a href="{{route('admin.item')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green">
          <div class="inner">
            <h3>{{$customer_total}}</h3>

            <p>Customers</p>
          </div>
          <div class="icon">
            <i class="ion ion-stats-bars"></i>
          </div>
          <a href="{{route('admin.customer')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
    <div class="col-sm-12">
      <div class="col-sm-6">
        <canvas id="barChart">
        </canvas>
        <h3 class="text-center">Bar Chart for Item</h3>
      </div>
      <div class="col-sm-6">
        <canvas id="pieChart">
        </canvas>
        <h3 class="text-center">Pie Chart for Item vs Customer</h3>
      </div>
    </section>
  </div>
  @endsection