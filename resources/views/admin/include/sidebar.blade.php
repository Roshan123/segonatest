  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('dist/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{Auth::user()->name}}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li >
          <a href="{{route('admin.home')}}">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
         <li >
          <a href="{{route('admin.item')}}">
            <i class="fa fa-dashboard"></i><span>Add Item</span>
          </a>
        </li>
         <li >
          <a href="{{route('admin.customer')}}">
            <i class="fa fa-dashboard"></i><span>Add Customer</span>
          </a>
        </li>
      </ul>
    </section>
  </aside>
