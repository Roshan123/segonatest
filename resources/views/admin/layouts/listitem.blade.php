<table id="datamodification" class="table table-bordered table-striped">
 <thead>
   <tr>
     <th>S.N</th>
     <th>Name</th>
     <th>Description</th>
     <th>Created At</th>
     <th>Image</th>
     <th>Action</th>
   </tr>
 </thead>
 <tbody>
  @foreach($items as $key=>$item)
  <tr>
   <td>{{++$key}}</td>
   <td>{{$item->name}}</td>
   <td>{{$item->description}}</td>
   <td>{{$item->created_at_date}}</td>
   <td><img src="{{asset("uploads/$item->image")}}" class="image-rounded imageposition"/></td>
   <td> 
     <a href="{{route('admin.item.edit',$item->id)}}"><button class="btn btn-warning btn-sm"  title="Edit"><i class="fa fa-edit"></i></button></a>
     <button class="btn btn-danger btn-sm"  title="Delete" data-toggle="modal" data-target="#deleteItem{{$item->id}}"><i class="fa fa-trash"></i></button></td>
   </tr>
 </div>
 <div id="deleteItem{{$item->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
   
        <div class="modal-body">
        <h3 class="text-center">Are you sure you want to delete?</h3>
        </div>
         <form action="{{route('admin.item.destroy',$item->id)}}" method="POST">
             {{csrf_field()}}
             {{method_field('DELETE')}}
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger"><i class="fa fa-save"></i> Delete</button>
          <button type="button" class="btn btn-default btn-danger-in" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
        </div>
      </form>
    </div>
  </div>
 </div>     
 @endforeach
</tbody>
</table>