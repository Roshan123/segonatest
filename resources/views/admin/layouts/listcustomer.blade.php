<table id="datamodification" class="table table-bordered table-striped">
 <thead>
   <tr>
     <th>S.N</th>
     <th>Name</th>
     <th>Item</th>
     <th>Email</th>
     <th>Contact Number</th>
     <th>Gender</th>
     <th>Action</th>
   </tr>
 </thead>
 <tbody>
  @foreach($customers as $key=>$customer)
  <tr>
   <td>{{++$key}}</td>
   <td>{{$customer->name}}</td>
   <td>{{$customer->item->name}}</td>
   <td>{{$customer->email}}</td>
   <td>{{$customer->contact_number}}</td>
   <td>{{$customer->gender}}</td>
   <td> 
     <a href="{{route('admin.customer.edit',$customer->id)}}"><button class="btn btn-warning btn-sm"  title="Edit"><i class="fa fa-edit"></i></button></a>
     <button class="btn btn-danger btn-sm"  title="Delete" data-toggle="modal" data-target="#deletecustomer{{$customer->id}}"><i class="fa fa-trash"></i></button></td>
   </tr>
 </div>
 <div id="deletecustomer{{$customer->id}}" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
   
        <div class="modal-body">
        <h3 class="text-center">Are you sure you want to delete?</h3>
        </div>
         <form action="{{route('admin.customer.destroy',$customer->id)}}" method="POST">
             {{csrf_field()}}
             {{method_field('DELETE')}}
        <div class="modal-footer">
          <button type="submit" class="btn btn-danger"><i class="fa fa-save"></i> Delete</button>
          <button type="button" class="btn btn-default btn-danger-in" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
        </div>
      </form>
    </div>
  </div>
 </div>     
 @endforeach
</tbody>
</table>