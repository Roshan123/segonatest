@extends('admin.layouts.main')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      General Form Elements
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Add Item</h3>
        </div>
          <p id="response"></p>
        <form role="form"  class="form-horizontal" method="POST" action="{{route('admin.item.store')}}" enctype="multipart/form-data">
          {{csrf_field()}}
          <div class="box-body">
            <div class="form-group">
              <label  class="col-sm-2 control-label">Name</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name"  placeholder="Enter Name">
                @if($errors->has('name'))<p id="error">{{$errors->first('name')}}</p>@endif
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Description</label>
              <div class="col-sm-8">
               <textarea id="description"  rows="10" cols="80"  name="description" placeholder="Enter description...">
              </textarea>
              @if($errors->has('description'))<p id="error">{{$errors->first('description')}}</p>@endif
            </div>
            </div>
          </div>
          <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Submission date</label>
             <div class="col-sm-8">
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" class="form-control pull-right" id="created_at_date" name="created_at_date" >
              @if($errors->has('created_at_date'))<p id="error">{{$errors->first('created_at_date')}}</p>@endif
            </div>
          </div>
        </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">Select Image</label>
            <div class=" col-sm-8">
              <input type="file"  name="image" class="form-control">
              @if($errors->has('image'))<p id="error">{{$errors->first('image')}}</p>@endif
            </div>
          </div>
        </div>
        <div class="form-group">
          <div class="box-footer">
            <button type="submit" class="btn btn-primary col-sm-offset-2">Submit</button>
          </div>
        </div>
      </form>
    </div>
    </div>
  </div>
</section>
</div>
@endsection

