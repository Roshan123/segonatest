@extends('admin.layouts.main')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      General Form Elements
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Item</h3>
        </div>
        <p id="response"></p>
        <form role="form"  class="form-horizontal" method="POST" action="{{route('admin.item.update',$item->id)}}" enctype="multipart/form-data">
          {{csrf_field()}}
          {{method_field('PUT')}}
          <div class="box-body">
            <div class="form-group">
              <label  class="col-sm-2 control-label">Name</label>
              <div class="col-sm-8">
                <input type="text" class="form-control"  name="name"  placeholder="Enter Name" value="{{$item->name}}">
                @if($errors->has('name'))<p id="error">{{$errors->first('name')}}</p>@endif
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Description</label>
              <div class="col-sm-8">
                <textarea class="textarea" placeholder="Enter description..."  name="description"
                style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                {{$item->description}}
              </textarea>
              @if($errors->has('description'))<p id="error">{{$errors->first('description')}}</p>@endif
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Submission date</label>
          <div class="col-sm-8">
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" class="form-control pull-right" value="{{$item->created_at_date}}"" name="created_at_date" >
              @if($errors->has('created_at_date'))<p id="error">{{$errors->first('created_at_date')}}</p>@endif
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-2 control-label">Select Image</label>
          <div class=" col-sm-8">
            <input type="file"  name="image" class="form-control">
            <img src="{{asset("/uploads/$item->image")}}" class="img-responsive imageposition">
            @if($errors->has('image'))<p id="error">{{$errors->first('image')}}</p>@endif
          </div>
        </div>
      </div>
      <div class="form-group">
        <div class="box-footer">
          <button type="submit" class="btn btn-primary col-sm-offset-2">Submit</button>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
</section>
</div>
@endsection


