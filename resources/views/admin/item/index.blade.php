@extends('admin.layouts.main')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Items</li>
    </ol>
  </section>
  <section class="content">
    <div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Search</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
      </div>
      <div class="box-body">
        <input type="text" placeholder="Search.." id="search">
        <button type="submit" id="button"><i class="fa fa-search" id="search_item"></i></button>
      </div>
    </div>
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
    <div class="row container-fluid">
      <a href="{{route('admin.item.create')}}">  <button type="button" class="btn btn-success btn-flat">Create Item</button></a>
    </div>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @include('admin.layouts.listitem')
        </div>
      </div>
    </section>
  </section>
</div>
@endsection
