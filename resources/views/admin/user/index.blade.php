@extends('admin.layouts.main')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
      <section class="content-header">
      <h1>
        Advanced Form Elements
        <small>Preview</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Forms</a></li>
        <li class="active">Advanced Elements</li>
      </ol>
    </section>
    <section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Search</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
          </div>
        </div>
      </div>
      <section class="content-header">
        <h1>
          Data Tables
          <small>advanced tables</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="#">Tables</a></li>
          <li class="active">Data tables</li>
        </ol>
      </section>
      <div class="row container-fluid">
      <a href="{{route('admin.user.create')}}">  <button type="button" class="btn btn-success btn-flat">Create user</button></a>
  </div>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
        	<table id="example1" class="table table-bordered table-striped">
        	  <thead>
        	  <tr>
        	    <th>S.N</th>
        	    <th>Name</th>
        	    <th>Email</th>
        	    <th>Image</th>
        	    <th>Action</th>
        	  </tr>
        	  </thead>
        	  <tbody>
              @foreach($users as $key=>$user)
        	  <tr>
        	    <td>{{++$key}}</td>
        	    <td>{{$user->name}}
        	    </td>
        	    <td>{{$user->email}}</td>
        	    <td><img src="{{asset("uploads/$user->image")}}" class="image-rounded imageposition"/></td>
        	    <td> 
              <button class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-edit"></i></button>
              <button class="btn btn-danger btn-sm" data-id="{{$user->id}}" id="userDelete{{$user->id}}" title="Delete"><i class="fa fa-trash"></i></button></td>
        	  </tr>
           {{--  <div class="modal fade" id="delete{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <h4 class="text-center">Are you sure you want to delete?</h4>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" value="{{$user->id}}" id="userDelete">Delete</button>
                  </div>
                </div>
              </div>
            </div> --}}
            @endforeach
        	  </tbody>
        	 </table>
        	</div>
        </div>
    </section>
  </section>
</div>
@endsection
