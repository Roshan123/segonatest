@extends('admin.layouts.main')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      General Form Elements
      <small>Preview</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Forms</a></li>
      <li class="active">General Elements</li>
    </ol>
  </section>
      <section class="content">
        <div class="row">
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Add User</h3>
              </div>
              <form role="form" class="form-horizontal" action="{{route('admin.user.store')}}" method="POST" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="box-body">
                    <div class="form-group">
                    <label  class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-8">
                    <input type="text" class="form-control" id="exampleInputEmail1"  name="name"placeholder="Enter Name">
                    @if($errors->has('name'))<p id="error">{{$errors->first('name')}}</p>@endif
                  </div>
                  </div>
                    <div class="form-group">
                    <label class="col-sm-2 control-label">Email address</label>
                    <div class="col-sm-8">
                    <input type="email" class="form-control" id="exampleInputEmail1" name="email" placeholder="Enter email">
                    @if($errors->has('email'))<p id="error">{{$errors->first('email')}}</p>@endif
                  </div>
                  </div>
                   <div class="form-group">
                    <label  class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="exampleInputPassword1" name="password" placeholder="Password">
                    @if($errors->has('password'))<p id="error">{{$errors->first('password')}}</p>@endif
                  </div>
                  </div>
                   <div class="form-group">
                    <label  class="col-sm-2 control-label"> Confirm Password</label>
                  <div class="col-sm-8">
                    <input type="password" class="form-control" id="exampleInputPassword1"  name="confirm_password" placeholder="Confirm Password">
                    @if($errors->has('confirm_password'))<p id="error">{{$errors->first('confirm_password')}}</p>@endif
                  </div>
                  </div>
                   <div class="form-group">
                    <label class="col-sm-2 control-label">Select Image</label>
                  <div class=" col-sm-8">
                    <input type="file" id="exampleInputFile" name="image">
                    @if($errors->has('image'))<p id="error">{{$errors->first('image')}}</p>@endif
                  </div>
                  </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary col-sm-offset-2">Submit</button>
                </div>
              </form>
            </div>
        </div>
    </section>
</div>
@endsection
