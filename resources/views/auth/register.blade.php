@extends('layouts.app')
@section('content')
    <body>
        <div class="container">
            <div class="row main">
                <div class="main-login main-center">
                    <p id="response"></p>
                    <form role="form"  class="form-horizontal" >                        
                        <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label">Your Name</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="name" id="name"  placeholder="Enter your Name"/>
                                    @if($errors->has('name'))<p id="error">{{$errors->first('name')}}</p>@endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="cols-sm-2 control-label">Your Email</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="email" id="email"  placeholder="Enter your Email"/>
                                    @if($errors->has('email'))<p id="error">{{$errors->first('email')}}</p>@endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="cols-sm-2 control-label">Password</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control" name="password" id="password"  placeholder="Enter your Password"/>
                                     @if($errors->has('password'))<p id="error">{{$errors->first('password')}}</p>@endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="confirm" class="cols-sm-2 control-label">Confirm Password</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control"  id="confirm_password"  placeholder="Confirm your Password"/>
                                     @if($errors->has('confirm_password'))<p id="error">{{$errors->first('confirm_password')}}</p>@endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <button type="button" id="register" class="btn btn-primary btn-sm btn-block login-button">Register</button>
                        </div>
                        <div class="login-register">
                            <a href="/"><h3>Login</h3></a>
                         </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
@endsection
